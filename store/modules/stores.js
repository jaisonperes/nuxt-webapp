import { api } from '../../api/axios'

export const NAMESPACE = 'stores'

// Getters
export const STORES = 'stores'
export const STORE = 'store'
export const ISLOADING = 'isLoading'

// Actions
export const GET_STORES = 'getStores'
export const GET_STORE = 'getStore'

// Mutations
export const SET_STORES = 'setStores'
export const SET_STORE = 'setStore'
export const SET_ISLOADING = 'setIsLoading'

// initial state
const state = {
  stores: [],
  store: {},
  isLoading: false
}

// getters
const getters = {
  [STORES]: (state) => {
    return state.stores
  },
  [STORE]: (state) => {
    return state.store
  },
  [ISLOADING]: (state) => {
    return state.isLoading
  }
}

// actions
const actions = {
  /**
   * [GET_STORES] - Retrieve stores
   *
   * @param  {function} commit Function to call mutations
   * @param  {Object}   state  Object with the actual state of the module
   */
  async [GET_STORES] ({ commit, dispatch, state, getters }, { sortBy, limit, search }) {
    try {
      commit(SET_ISLOADING, true)
      const params = { sortBy, limit, search: JSON.stringify(search) }
      const { data } = await api.get('/stores', { params })
      if (data && data.items && data.items.length > 0) {
        commit(SET_STORES, data.items)
      }
    } finally {
      commit(SET_ISLOADING, false)
    }
  },
  /**
   * [GET_STORE] - Retrieve store
   *
   * @param  {function} commit Function to call mutations
   * @param  {Object}   state  Object with the actual state of the module
   */
  async [GET_STORE] ({ commit, dispatch, state, getters }, { storeId }) {
    try {
      commit(SET_ISLOADING, true)
      const { data } = await api.get('/stores/' + storeId)
      commit(SET_STORE, data)
    } finally {
      commit(SET_ISLOADING, false)
    }
  }
}

// mutations
const mutations = {
  [SET_STORES] (state, stores) {
    state.stores = stores
  },
  [SET_STORE] (state, store) {
    state.store = store
  },
  [SET_ISLOADING] (state, isLoading) {
    state.isLoading = isLoading
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
