import { api } from '../../api/axios'

export const NAMESPACE = 'products'

// Getters
export const PRODUCTS = 'products'
export const PRODUCT = 'product'
export const ISLOADING = 'isLoading'

// Actions
export const GET_PRODUCTS = 'getProducts'
export const GET_PRODUCT = 'getProduct'
export const UPDATE_PRODUCT = 'updateproduct'

// Mutations
export const SET_PRODUCTS = 'setProducts'
export const SET_PRODUCT = 'setProduct'
export const SET_ISLOADING = 'setIsLoading'

// initial state
const state = {
  products: [],
  product: {},
  isLoading: false
}

// getters
const getters = {
  [PRODUCTS]: (state) => {
    return state.products
  },
  [PRODUCT]: (state) => {
    return state.product
  },
  [ISLOADING]: (state) => {
    return state.isLoading
  }
}

// actions
const actions = {
  /**
   * [GET_PRODUCTS] - Retrieve products
   *
   * @param  {function} commit Function to call mutations
   * @param  {Object}   state  Object with the actual state of the module
   */
  async [GET_PRODUCTS] ({ commit, dispatch, state, getters }, { sortBy, limit, search }) {
    try {
      commit(SET_ISLOADING, true)
      const params = { sortBy, limit, search: JSON.stringify(search) }
      const { data } = await api.get('/products/', { params })
      if (data && data.items && data.items.length > 0) {
        commit(SET_PRODUCTS, data.items)
      }
    } finally {
      commit(SET_ISLOADING, false)
    }
  },
  /**
   * [GET_PRODUCT] - Retrieve product
   *
   * @param  {function} commit Function to call mutations
   * @param  {Object}   state  Object with the actual state of the module
   */
  async [GET_PRODUCT] ({ commit, dispatch, state, getters }, { productId }) {
    try {
      commit(SET_ISLOADING, true)
      const url = '/products/' + productId
      const { data } = await api.get(url)
      commit(SET_PRODUCT, data)
    } finally {
      commit(SET_ISLOADING, false)
    }
  }
}

// mutations
const mutations = {
  [SET_PRODUCTS] (state, products) {
    state.products = products
  },
  [SET_PRODUCT] (state, product) {
    state.product = product
  },
  [SET_ISLOADING] (state, isLoading) {
    state.isLoading = isLoading
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
