import Vue from 'vue'
import Vuex from 'vuex'

import products from './modules/products.js'
import stores from './modules/stores.js'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    modules: {
      products,
      stores
    }
  })

  return Store
}
