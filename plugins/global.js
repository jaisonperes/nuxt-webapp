import Vue from 'vue'
import Spinner from '../components/ui/Spinner'
import Btn from '../components/ui/Btn'

Vue.component('spinner', Spinner)
Vue.component('btn', Btn)
